//
//  VolumeController.m
//  AudioZen
//
//  Created by r on 18.12.10.
//  Copyright 2010 - 2011 Tailorama. All rights reserved.
//

#import "VolumeController.h"

@implementation VolumeController
@synthesize repeatingTimer;

- (IBAction) adjustVolume: (id) sender {
  NSLog(@"*info* Controller.adjustVolume() - start.");
  // initialise a volume model object.
  volume = [[Volume alloc]init];
  
  // sets the model value according to the current slider value.
  [volume setCurrentVolume:[volumeSlider floatValue]];
  [volume adjustVolume:[volume currentVolume]];
  NSLog(@"*info* Controller.adjustVolume() - end.");  
}

- (void) initPreviousVolume {
  volume = [[Volume alloc]init];
  [self determineSystemVolume];
}

- (void) determineSystemVolume {
    NSLog(@"*info* Controller.determineSystemVolume() - start.");
  // retrieve the system's current volume.
  VolumeValues* systemVolume = [volume determineCurrentSystemVolume];
  
  // adjust the slider.
  if (systemVolume.hasMasterVolume) {
	[volumeSlider setFloatValue:([systemVolume masterVolume] * 100.0)];	
  }
  else {
	float left = [systemVolume leftVolume] * 100.0;
	float right = [systemVolume rightVolume] * 100.0;
	float master = (left + right) / 2.0;
	[volumeSlider setFloatValue:master];
  }
  NSLog(@"*info* Controller.determineSystemVolume() - end.");
}

// Initialise the context data for the timer.
- (NSDictionary *) userInfo {
  return [NSDictionary dictionaryWithObject:[NSDate date] forKey:@"StartDate"];
}

// Start the timer and let it fire every 0.5 seconds.
- (IBAction)startRepeatingTimer:sender {
  NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.5
													target:self selector:@selector(timerFireMethod:)
												  userInfo:[self userInfo]
												   repeats:YES];
  self.repeatingTimer = timer;
}

// Stop the repeating timer.
- (IBAction)stopRepeatingTimer:sender {
  [repeatingTimer invalidate];
  self.repeatingTimer = nil;
}

// Method to run when the timer fires.
- (void)timerFireMethod:(NSTimer*)theTimer {
  NSLog(@"*info* timerFiredMethod was called!");
  // TODO determineSystemVolume;
}

// Lets act as a delegate for NSApplication in order to get the timer started.
- (void)applicationDidFinishLaunching:(NSNotification *)notification {
  [self initPreviousVolume];
  [self startRepeatingTimer:self];
}


@end
