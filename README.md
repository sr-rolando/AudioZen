# AudioZen

Exponential volume control for sensitive ears.

Do you dream of a simple, yet powerful, hifi setup?

Do you prefere a fine tune instead of some screaming noise?

Do you want to be in charge of pleasing your ears?

Then AudioZen might be for you.

It provides exponential volume control for your sensitive ears. This is especially useful for people with fine active speakers connected to their Mac via a highend DAC like the [D-Linear](http://www.opera-consonance.com/products/digital/D-linear8.htm) by Opera Consonance.

## Development

To build the app:

1. Open `audiozen.xcodeproj` in XCode
2. Product / Build (cmd+B)
3. Product / Archive / Distribute App / Copy App
