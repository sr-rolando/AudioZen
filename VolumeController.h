//
//  VolumeController.h
//  AudioZen
//
//  Created by r on 18.12.10.
//  Copyright 2010 - 2011 Tailorama. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Volume.h"

@interface VolumeController : NSObjectController {
  IBOutlet id volumeSlider;
  NSTimer *__weak repeatingTimer;
  Volume *volume;
}

- (IBAction) adjustVolume: (id) sender;

// To be called just once - when the system starts.
// Initialises the volume pointer and sets the slider to the current system volume (w/o altering it).
- (void) initPreviousVolume;

// Gets the current system volume and adjusts the volume slider to it's value.
// Doesn't alter the system value but just retrieves it and sets the slider's value accordingly.
- (void) determineSystemVolume;

// The timer used for listening on the system volume.
@property (weak) NSTimer *repeatingTimer;

// The timer that runs repetively, checking the current volume and adjusting the slider accordingly.
- (IBAction)startRepeatingTimer:sender;
- (IBAction)stopRepeatingTimer:sender;

// Method to run when the timer fires.
- (void)timerFireMethod:(NSTimer*)theTimer;

// Some context data for the timer.
- (NSDictionary *)userInfo;


// Lets act as a delegate for NSApplication in order to get the timer started.
- (void)applicationDidFinishLaunching:(NSNotification *)notification;

@end
