//
//  VolumeValues.m
//  AudioZen
//
//  Created by r on 28.12.10.
//  Copyright 2010 - 2011 Tailorama. All rights reserved.
//

#import "VolumeValues.h"

@implementation VolumeValues

@synthesize hasMasterVolume;
@synthesize masterVolume;
@synthesize leftVolume;
@synthesize rightVolume;

@end
