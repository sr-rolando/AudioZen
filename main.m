//
//  main.m
//  AudioZen
//
//  Created by r on 14.12.10.
//  Copyright Tailorama 2010-2011. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
