//
//  Volume.h
//  AudioZen
//
//  Created by r on 16.12.10.
//  Copyright 2010 - 2011 Tailorama. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <CoreAudio/CoreAudio.h>
#import "VolumeValues.h"

@interface Volume : NSObject {
  float currentVolume;
  AudioDeviceID mOutputDevice;
}

@property(readwrite) float currentVolume;

- (float) adjustVolume: (float) newValue;
- (float) calcVolume: (float) newValue;
- (void) determineOutputDevice;
- (VolumeValues *) determineCurrentSystemVolume;

@end
