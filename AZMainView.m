//
//  AZMainView.m
//  AudioZen
//
//  Created by r on 25.12.10.
//  Copyright 2010 - 2011 Tailorama. All rights reserved.
//

#import "AZMainView.h"

@implementation AZMainView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)rect {
  // Do nothing for now, as this will gradually lead to a 0-volume.
  // TODO Better: adjust the value once at startup & listen in on all ways of a changing volume.
  //[volumeController determineSystemVolume];
}

@end
