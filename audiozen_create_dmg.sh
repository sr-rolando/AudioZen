# be sure to check the man page for hdiutil
# it is very powerful and has tons of options...

hdiutil create -megabytes 50 -fs HFS+ -volname AudioZen AudioZen.dmg
hdiutil mount AudioZen.dmg
cp AudioZen.pkg /Volumes/AudioZen/
