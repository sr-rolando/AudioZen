//
//  AZMainView.h
//  AudioZen
//
//  Created by r on 25.12.10.
//  Copyright 2010 - 2011 Tailorama. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "VolumeController.h"

@interface AZMainView : NSView {
  IBOutlet VolumeController* volumeController;
}

@end
