//
//  VolumeValues.h
//  AudioZen
//
//  Created by r on 28.12.10.
//  Copyright 2010 - 2011 Tailorama. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface VolumeValues : NSObject {
  bool hasMasterVolume;  // true if master volume is set, false if separate left/right channels are set.
  float masterVolume;
  float rightVolume;
  float leftVolume;
}

@property(readwrite) bool hasMasterVolume;
@property(readwrite) float masterVolume;
@property(readwrite) float rightVolume;
@property(readwrite) float leftVolume;

@end
