//
//  Volume.m
//  AudioZen
//
//  Created by r on 16.12.10.
//  Copyright 2010 - 2011 Tailorama. All rights reserved.
//

#import "Volume.h"

#import <AudioToolbox/AudioServices.h>
#import <AvailabilityMacros.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <CoreFoundation/CoreFoundation.h>
#import <math.h> // pow() with float results

@implementation Volume
@synthesize currentVolume;

- (float) adjustVolume: (float) newValue {
  float newVolume = [self calcVolume: newValue];
  Boolean isInput = false; // we are talking about output devices here.
  Boolean isWritable = false; // 'false' as conservative default value
  
  [self determineOutputDevice];

  // check the master channel first.
  UInt32 channel = 0;
  UInt32 size = sizeof(isWritable);
	// FIXME Probably use AudioObjectGetPropertyData() instead of AudioDeviceGetPropertyInfo()
	// see https://developer.apple.com/library/archive/technotes/tn2223/_index.html
	OSStatus res = AudioDeviceGetPropertyInfo(mOutputDevice, channel, isInput, kAudioDevicePropertyVolumeScalar, &size, &isWritable);
	if (noErr == res && true == isWritable) { // got control of the master channel
		UInt32 size = sizeof(newValue);
		// FIXME Use AudioObjectSetPropertyData instead.
		AudioDeviceSetProperty(mOutputDevice,
							   NULL, // time stamp not needed
							   channel, isInput, kAudioDevicePropertyVolumeScalar, size, &newVolume);
		return newValue;
	}
	else { // if master channel not supported, check separate channels.
		UInt32 channels[2];
		size = sizeof(channels);
		res = AudioDeviceGetProperty(mOutputDevice, 0, isInput, kAudioDevicePropertyPreferredChannelsForStereo, &size, &channels);
		if (noErr != res) {
		  NSLog(@"adjustVolume - error: %s, comment: %s (%d)", GetMacOSStatusErrorString(res), GetMacOSStatusCommentString(res), res);
		  return false;
		}
		// set volume on both channels.
		size = sizeof(newVolume);
		res = AudioDeviceSetProperty(mOutputDevice, 0, channels[0], isInput, kAudioDevicePropertyVolumeScalar, size, &newVolume);
		if(noErr != res) {
		  NSLog(@"adjustVolume - error setting volume of channel %d", channels[0]);
		}
		res = AudioDeviceSetProperty(mOutputDevice, 0, channels[1], isInput, kAudioDevicePropertyVolumeScalar, size, &newVolume);
		if(noErr != res) {
		  NSLog(@"adjustVolume - error setting volume of channel %d", channels[1]);
		}
	}
  
  self.currentVolume = newValue;
  return self.currentVolume;
}

- (float) calcVolume: (float) newValue {
  float basis = 1.07;
  float factor = 0.1154;
  float offset = factor;
  return ((pow(basis, newValue) * factor) - offset) / 100.0;
}

// sets mOutputDevice
- (void) determineOutputDevice {
  UInt32 size = sizeof(mOutputDevice);
  /* this new, not deprecated, API doesn't seem to return a usable output device id, but only one causing error 560227702 (kAudioHardwareBadDeviceError).
  // conservative default.
  mOutputDevice = kAudioObjectUnknown;
  
  AudioObjectPropertyAddress theAddress;
  theAddress.mScope = kAudioObjectPropertyScopeGlobal;
  theAddress.mElement = kAudioObjectPropertyElementMaster;
  theAddress.mSelector = kAudioHardwarePropertyDefaultOutputDevice;
  
  if (!AudioHardwareServiceHasProperty(kAudioObjectSystemObject, &theAddress)) {
	NSLog(@"determineOutputDevice - cannot find default output device. Aborting.");
	return;
  }
  
  OSStatus res = AudioHardwareServiceGetPropertyData(kAudioObjectSystemObject, &theAddress, 0, NULL, &size, &mOutputDevice);
  if (noErr != res) {
	NSLog(@"determineOutputDevice - cannot actually get the default output device.");
  }
  */
  // FIXME labeled as 'deprecated', should be using 'AudioObjectGetPropertyData'
  OSStatus res = AudioHardwareGetProperty(kAudioHardwarePropertyDefaultOutputDevice, &size, &mOutputDevice);
  if (res != noErr) {
	printf("determineOutputDevice - error: %s, comment: %s (%d)", GetMacOSStatusErrorString(res), GetMacOSStatusCommentString(res), res);
  }
}

- (VolumeValues *) determineCurrentSystemVolume {
  if (! mOutputDevice) {
	[self determineOutputDevice];
  }
  
  VolumeValues *ret = [[VolumeValues alloc] init];
  Boolean isInput = false;
  
  // check master channel first.
  UInt32 theChannel = 0;
  Float32 bufferForVolume;
  UInt32 size = sizeof(bufferForVolume);
  AudioObjectPropertyScope theScope = isInput ? kAudioDevicePropertyScopeInput : kAudioDevicePropertyScopeOutput;
  AudioObjectPropertyAddress theAddress = { kAudioDevicePropertyVolumeScalar, theScope, theChannel };
  OSStatus res = AudioObjectGetPropertyData(mOutputDevice, &theAddress, 0, NULL, &size, &bufferForVolume);
  if (noErr == res) {
	[ret setMasterVolume:bufferForVolume];
	[ret setLeftVolume:bufferForVolume];
	[ret setRightVolume:bufferForVolume];
	[ret setHasMasterVolume:true];
  }
  else { // if master channel did not work out, try the AudioToolbox framework.
	bool gotError = false;
	
	AudioObjectPropertyAddress address;
	theAddress.mElement = kAudioObjectPropertyElementMaster;
	theAddress.mSelector = kAudioHardwareServiceDeviceProperty_VirtualMasterVolume;
	theAddress.mScope = kAudioDevicePropertyScopeOutput;
	
	if (!AudioHardwareServiceHasProperty(mOutputDevice, &address)) {
	  NSLog(@"determineCurrentSystemVolume: no system volume available.");
	  gotError = true;
	}
	else {
	  res = AudioHardwareServiceGetPropertyData(mOutputDevice, &address, 0, NULL, &size, &bufferForVolume);
	  if (noErr != res) {
		NSLog(@"determineCurrentSystemVolume - errstring = %s, commentstr = %s, res = %d (left = %d)", GetMacOSStatusErrorString(res), GetMacOSStatusCommentString(res), res, bufferForVolume);
		return ret; 
	  }
	  
	  if (bufferForVolume < 0.0 || bufferForVolume > 1.0) {
		NSLog(@"determineCurrentSystemVolume: got some strange volume value of %d. Using 0.0 instead.", bufferForVolume);
		bufferForVolume = 0.0;
	  }
	  
	  // All seems OK.
	  [ret setMasterVolume:bufferForVolume];
	  [ret setLeftVolume:bufferForVolume];
	  [ret setRightVolume:bufferForVolume];
	  [ret setHasMasterVolume:true];
	}
	
	// Still not done? Try the (officially obsoleted) Good Ol' Way.
	if (gotError) {
	  isInput = false;
	  UInt32 channels[2];
	  size = sizeof(channels);
	  res = AudioDeviceGetProperty(mOutputDevice, 0, isInput, kAudioDevicePropertyPreferredChannelsForStereo, &size, &channels);
	  if (noErr != res) {
		NSLog(@"determineCurrentSystemVolume - getting channels. error: %s, comment: %s (%d)", GetMacOSStatusErrorString(res), GetMacOSStatusCommentString(res), res);
		return false;
	  }
	  // get volume on both channels.
	  Float32 bufferForLeftVolume;
	  size = sizeof(bufferForLeftVolume);
	  res = AudioDeviceGetProperty(mOutputDevice, channels[0], isInput, kAudioDevicePropertyVolumeScalar, &size, &bufferForLeftVolume);
	  if(noErr != res) {
		NSLog(@"determineCurrentSystemVolume - error getting volume of channel %d", channels[0]);
	  }
	  Float32 bufferForRightVolume;
	  size = sizeof(bufferForRightVolume);
	  res = AudioDeviceGetProperty(mOutputDevice, channels[1], isInput, kAudioDevicePropertyVolumeScalar, &size, &bufferForRightVolume);
	  if(noErr != res) {
		NSLog(@"determineCurrentSystemVolume - error getting volume of channel %d", channels[1]);
	  }
	  
	  bufferForVolume = (bufferForLeftVolume + bufferForRightVolume) / 2.0;
	  [ret setMasterVolume:bufferForVolume];
	  [ret setLeftVolume:bufferForVolume];
	  [ret setRightVolume:bufferForVolume];
	  [ret setHasMasterVolume:true];
	  NSLog(@"*info* got volume %f using separate channels (left = %f, right = %f).", (double) bufferForVolume, (double) bufferForLeftVolume, (double) bufferForRightVolume);
	}	
	
	
	
	/* left / right: not working == always gets 0-values:
	// check left channel.
	theChannel = 1;
	AudioObjectPropertyAddress theLeftAddress = { kAudioDevicePropertyVolumeScalar, theScope, theChannel };
	res = AudioObjectGetPropertyData(mOutputDevice, &theLeftAddress, 0, NULL, &size, &bufferForVolume);
	if (noErr == res) {
	  [ret setLeftVolume:bufferForVolume];
	  NSLog(@"determineCurrentSystemVolume - left = %d", bufferForVolume);
	}
	else {
	  NSLog(@"determineCurrentSystemVolume - errstring = %s, commentstr = %s, res = %d (left = %d)", GetMacOSStatusErrorString(res), GetMacOSStatusCommentString(res), res, bufferForVolume);
	}
	
	// check right channel.
	theChannel = 2;
	AudioObjectPropertyAddress theRightAddress = { kAudioDevicePropertyVolumeScalar, theScope, theChannel };
	res = AudioObjectGetPropertyData(mOutputDevice, &theRightAddress, 0, NULL, &size, &bufferForVolume);
	if (noErr == res) {
	  [ret setRightVolume:bufferForVolume];
	  NSLog(@"determineCurrentSystemVolume - right = %d", bufferForVolume);
	}
	// make sure to mark not having master volume but balance settings for left/right channel.
	[ret setHasMasterVolume:false];
	 */
  }
  
  return ret;
}

@end
